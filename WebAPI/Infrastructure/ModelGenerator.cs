﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using Domain.Entities;
using ExifLibrary;
using WebAPI.Exception;
using File = WebAPI.Models.File;

namespace WebAPI.Infrastructure
{
    public class ExifGenerator
    {
        /// <summary>
        /// Exif Metadata 객체에서 Exif Model을 생성합니다.
        /// </summary>
        /// <param name="exif">Exif Metadata입니다.</param>
        /// <returns>Exif Model입니다.</returns>
        private static Exif GenerateExif(ExifFile exif)
        {
            Exif imgExif = null;
            if (exif.Properties.Count > 0)
            {
                imgExif = new Exif();

                foreach (var property in exif.Properties.Values)
                {
                    // property에 따라 해당 변수에 데이터 삽입
                    switch (property.Tag)
                    {
                        case ExifTag.GPSLatitude:
                            imgExif.Latitude = (float)((GPSLatitudeLongitude)property);
                            break;

                        case ExifTag.GPSLongitude:
                            imgExif.Longitude = (float)((GPSLatitudeLongitude)property);
                            break;

                        case ExifTag.Make:
                            imgExif.CameraMaker = (string)property.Value;
                            break;

                        case ExifTag.Model:
                            imgExif.CameraModel = (string)property.Value;
                            break;

                        case ExifTag.LensMake:
                            imgExif.LensMake = (string)property.Value;
                            break;

                        case ExifTag.LensModel:
                            imgExif.LensModel = (string)property.Value;
                            break;

                        case ExifTag.Orientation:
                            imgExif.Orientation = property.Value.ToString();
                            break;

                        case ExifTag.ExposureTime:
                            imgExif.ExposureTime = property.Value.ToString();
                            break;

                        case ExifTag.FNumber:
                            imgExif.FNumber = property.Value.ToString();
                            break;

                        case ExifTag.MaxApertureValue:
                            imgExif.MaxApertureValue = property.Value.ToString();
                            break;

                        case ExifTag.Flash:
                            imgExif.FlashMode = property.Value.ToString();
                            break;

                        case ExifTag.MeteringMode:
                            imgExif.MeteringMode = property.Value.ToString();
                            break;

                        case ExifTag.FocalLength:
                            imgExif.FocalLength = property.Value.ToString();
                            break;

                        case ExifTag.ISOSpeedRatings:
                            imgExif.Iso = property.Value.ToString();
                            break;

                        case ExifTag.WhiteBalance:
                            imgExif.WhiteBalance = property.Value.ToString();
                            break;

                        case ExifTag.DateTime:
                            imgExif.Datetime = (DateTime)property.Value;
                            break;
                    }
                }
            }

            return imgExif;
        }

        public static Exif Generate(File model)
        {
            if (model.MediaType != "image/jpeg")
                throw new NotSupportedMediaTypeException();

            var exif = ExifFile.Read(model.Data);
#if DEBUG
            foreach (var property in exif.Properties)
                Trace.WriteLine($"{property.Key}: {property.Value}");
#endif
            Exif imgExif = GenerateExif(exif);
            return imgExif;
        }
    }

    public class EntityGenerator<T>
    {
        public static readonly string MEDIATYPE_IMAGE = "image";
        public static readonly string MEDIATYPE_VIDEO = "video";

        public string PropertyName_FileList { get; private set; }

        private List<string> _acceptMediaTypes = new List<string>();

        public EntityGenerator()
        {
            PropertyName_FileList = "Files";
            _acceptMediaTypes.Add(MEDIATYPE_IMAGE);
        }
        
        private string GetFileName(string path)
        {
            string[] token = path.Split('\\');
            return token[token.Length - 1];
        }

        private void Write(PropertyInfo p, object inst, object val)
        {
            Type pType = p.PropertyType;
            if (pType == typeof (byte))
                p.SetValue(inst, Convert.ToByte(val));
            else if (pType == typeof (char))
                p.SetValue(inst, Convert.ToChar(val));
            else if (pType == typeof (bool))
                try
                {
                    p.SetValue(inst, Convert.ToBoolean(val));
                }
                catch (FormatException ex)
                {
                    p.SetValue(inst, false);
                }
            else if (pType == typeof (short))
                p.SetValue(inst, Convert.ToInt16(val));
            else if (pType == typeof (int))
                p.SetValue(inst, Convert.ToInt32(val));
            else if (pType == typeof (long))
                p.SetValue(inst, Convert.ToInt64(val));
            else if (pType == typeof (ushort))
                p.SetValue(inst, Convert.ToUInt16(val));
            else if (pType == typeof (uint))
                p.SetValue(inst, Convert.ToUInt32(val));
            else if (pType == typeof (ulong))
                p.SetValue(inst, Convert.ToUInt64(val));
            else if (pType == typeof (float))
                p.SetValue(inst, Convert.ToSingle(val));
            else if (pType == typeof (double))
                p.SetValue(inst, Convert.ToDouble(val));
            else if (pType == typeof (string))
                p.SetValue(inst, Convert.ToString(val));
            else if (pType == typeof (DateTime))
                // C#: Datetime <-> JAVA: Calendar 데이터는 yyyy-MM-dd HH:mm:ss format string으로 주고받는다.
                p.SetValue(inst,
                    DateTime.ParseExact(Convert.ToString(val), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture));
            else
                p.SetValue(inst, val);
        }

        public void SetPropertyFileName(string propertyName)
        {
            PropertyName_FileList = propertyName;
        }

        public void AddApprovalMediaType(string mediatype)
        {
            if (_acceptMediaTypes.Count(a => a == mediatype) == 0)
                _acceptMediaTypes.Add(mediatype);
        }

        public void RemoveApprovalMeidaType(string mediatype)
        {
            _acceptMediaTypes.Remove(mediatype);
        }

        public async Task<T> FromMultipartFormData(HttpContent content, Type model)
        {
            // 먼저 content의 enctype이 Multipart/Form-Data인지 확인한다.
            if (!content.IsMimeMultipartContent())
                throw new NotSupportedMimeTypeException("This request enctype is not multypart/form-data");

            // provider를 통해 form-data를 읽어낸다.
            var provider = new MultipartMemoryStreamProvider();
            await content.ReadAsMultipartAsync(provider);

            // model의 read, write가 가능한 property를 읽어낸다.
            // 모든 model은 모든 필드에 대한 get, set 프로퍼티를 설정해야 한다.
            var properties = model.GetProperties().Where(p => p.CanRead && p.CanWrite);

            // model을 인스턴스화 한다.
            T instance = (T)Activator.CreateInstance(model);
            List<File> fileList = new List<File>();
            foreach (var c in provider.Contents)
            {
                // content length가 0이라면 값이 없는 것이니 무시한다.
                if (c.Headers.ContentLength == 0)
                    continue;

                // fileName이 null이거나 empty라면 일반 form-data일 것.
                if (string.IsNullOrEmpty(c.Headers.ContentDisposition.FileName))
                {
                    string formName = c.Headers.ContentDisposition.Name.Trim('"');
                    // 대소문자 상관없이 property name과 form name이 일치하는 프로퍼티를 대상으로 작업한다.
                    var property = properties.SingleOrDefault(p => string.Equals(p.Name, formName, StringComparison.CurrentCultureIgnoreCase));
                    if (property == null)
                        continue;

                    string val = await c.ReadAsStringAsync();
                    Write(property, instance, val);
                }
                else
                {   // filename이 존재한다면 raw data일 것.
                    // mediatype을 검사한다.
                    string mediaType = c.Headers.ContentType.MediaType.Trim('"');
                    string[] mediaTypeTokens = mediaType.Split('/');

                    // 허가된 미디어 타입인지 검사한다.
                    if (_acceptMediaTypes.Count(a => a == mediaTypeTokens[0]) == 0)
                        throw new NotSupportedMediaTypeException();

                    byte[] raw = await c.ReadAsByteArrayAsync();
                    File file = new File();
                    file.Data = raw;
                    file.FileName = GetFileName(c.Headers.ContentDisposition.FileName.Trim('"').Trim('\\'));
                    file.MediaType = mediaType;
                    file.FileSize = (int)(c.Headers.ContentLength ?? raw.Length);

                    using (var img = Image.FromStream(new MemoryStream(raw)))
                    {
                        file.Width = img.Width;
                        file.Height = img.Height;
                    }

                    fileList.Add(file);
                }
            }
            // property중 Files라는 프로퍼티가 있는지 찾는다.
            var filesProperty = properties.SingleOrDefault(p => string.Equals(p.Name, PropertyName_FileList, StringComparison.CurrentCultureIgnoreCase));
            // 있다면 fileList를 삽입한다.
            filesProperty?.SetValue(instance, fileList);
            return instance;
        }
    }
}