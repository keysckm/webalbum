﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Infrastructure
{
    public class TokenUtil
    {
        public static string GenerateToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}