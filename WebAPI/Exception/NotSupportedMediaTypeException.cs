﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Exception
{
    public class NotSupportedMediaTypeException : System.Exception
    {
        public NotSupportedMediaTypeException()
            : base("Not supported image type")
        {
            
        }

        public NotSupportedMediaTypeException(string msg)
            : base(msg)
        {
            
        }
    }
}
