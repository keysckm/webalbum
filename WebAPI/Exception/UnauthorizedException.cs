﻿namespace WebAPI.Exception
{
    public class UnauthorizedException : System.Exception
    {
        public UnauthorizedException()
            : base("Unauthorized Request")
        {
            
        }

        public UnauthorizedException(string msg)
            : base(msg)
        {
            
        }
    }
}