﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Exception
{
    public class NotSupportedMimeTypeException : System.Exception
    {

        public NotSupportedMimeTypeException()
            : base("Not Supported Mime Type")
        {
            
        }

        public NotSupportedMimeTypeException(string msg)
            : base(msg)
        {
            
        }
    }
}