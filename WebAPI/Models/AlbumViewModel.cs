﻿using System.ComponentModel.DataAnnotations;

namespace WebAPI.Models
{
    namespace Request
    {
        public class CreateAlbum : UploadFile
        {
            [Required]
            [StringLength(100)]
            public string Title { get; set; }
        }

        public class ModifyAlbum : BasicAuthModel
        {
            public int Idx { get; set; }
            [Required]
            [StringLength(100)]
            public string Title { get; set; }
        }
    }

    namespace Response
    {
        public class AlbumModel
        {
            public int Idx { get; set; }
            public string Title { get; set; }
            public int ImageCount { get; set; }
            public string Date { get; set; }
            public int Thumbnail { get; set; }
        }
    }
}