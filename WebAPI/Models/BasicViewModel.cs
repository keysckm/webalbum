﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    namespace Request
    {
        public class BasicAuthModel
        {
            [Required]
            [StringLength(64)]
            [MinLength(8)]
            public string Id { get; set; }

            [Required]
            public string Token { get; set; }
        }
    }
}