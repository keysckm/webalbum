﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    namespace Request
    {
        /// <summary>
        /// 로그인 Request 모델
        /// </summary>
        public class LoginModel
        {
            [Required]
            [StringLength(64)]
            [MinLength(8)]
            [EmailAddress]
            public string Id { get; set; }

            [Required]
            [StringLength(32)]
            public string Pw { get; set; }
        }

        /// <summary>
        /// 로그아웃 Request 모델
        /// </summary>
        public class LogoutModel
        {
            [Required]
            [StringLength(64)]
            [MinLength(8)]
            [EmailAddress]
            public string Id { get; set; }

            [Required]
            public string Token { get; set; }
        }

        /// <summary>
        /// 회원가입 Request 모델
        /// </summary>
        public class UserModel
        {
            [Required]
            [StringLength(64)]
            [MinLength(8)]
            [EmailAddress]
            public string Id { get; set; }

            [Required]
            [StringLength(32)]
            public string Pw { get; set; }

            [Required]
            [StringLength(16)]
            public string Nickname { get; set; }
        }
    }

    namespace Response
    {
        /// <summary>
        /// 로그인 성공시 유저 정보 response 모델
        /// </summary>
        public class LoginResult
        {
            public int Idx { get; set; }
            public string Id { get; set; }
            public string Nickname { get; set; }
            public string Token { get; set; }
        }
    }
}