﻿using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public struct File
    {
        public string FileName { get; set; }
        public string MediaType { get; set; }
        public int FileSize { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public byte[] Data { get; set; }
    }

    namespace Request
    {
        public class GetImageInfos : BasicAuthModel
        {
            public int AlbumIdx { get; set; }
        }

        public class GetImage : BasicAuthModel
        {
            public int ImageIdx { get; set; }
        }

        public class GetExif : BasicAuthModel
        {
            public int ImageIdx { get; set; }
        }

        public class UploadFile : BasicAuthModel
        {
            public List<File> Files { get; set; }
            // public string FileName { get; set; }
            // public string MediaType { get; set; }
            // public int FileSize { get; set; }
            // public byte[] File { get; set; }
        }

        public class UploadPicture : UploadFile
        {
            public int Album { get; set; }
            public string Comment { get; set; }
        }

        public class UpdateExif
        {
            public int ImageIdx { get; set; }           // 이미지 식별자. Exif 식별자가 아님에 주의할 것.
            public float? Latitude { get; set; }        // 위도
            public float? Longitude { get; set; }       // 경도
            public string CameraMaker { get; set; }     // 카메라 제조사
            public string CameraModel { get; set; }     // 카메라 모델
            public string LensMaker { get; set; }       // 렌즈 제조사
            public string LensModel { get; set; }       // 렌즈 모델
            public string Orientation { get; set; }     // 사진 방향
            public string ExposureTime { get; set; }    // 셔터스피드
            public string FNumber { get; set; }         // 조리개
            public string MaxApertureValue { get; set; }// 조리개 최대 개방
            public string FlashMode { get; set; }       // 플래시 모드
            public string MeteringMode { get; set; }    // 측광 모드
            public string FocalLength { get; set; }     // 초점 거리
            public string Iso { get; set; }             // ISO
            public string WhiteBalance { get; set; }    // 화이트밸런스
            public string Datetime { get; set; }        // 촬영 시각
        }

#if DEBUG
        public class TestModel : UploadFile
        {
            public int Int1 { get; set; }
            public float Float1 { get; set; }
            public char Char1 { get; set; }
            public bool Bool1 { get; set; }
        }
#endif
    }

    namespace Response
    {
        public class ImageInfo
        {
            public int Idx { get; set; }
            public string Comment { get; set; }
            public int ThumbnailIdx { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string FileName { get; set; }
            public int FileSize { get; set; }
        }

        public class ExifInfo
        {
            public int Idx { get; set; }                // 식별자
            public float? Latitude { get; set; }        // 위도
            public float? Longitude { get; set; }       // 경도
            public string CameraMaker { get; set; }     // 카메라 제조사
            public string CameraModel { get; set; }     // 카메라 모델
            public string LensMaker { get; set; }       // 렌즈 제조사
            public string LensModel { get; set; }       // 렌즈 모델
            public string Orientation { get; set; }     // 사진 방향
            public string ExposureTime { get; set; }    // 셔터스피드
            public string FNumber { get; set; }         // 조리개
            public string MaxApertureValue { get; set; }// 조리개 최대 개방
            public string FlashMode { get; set; }       // 플래시 모드
            public string MeteringMode { get; set; }    // 측광 모드
            public string FocalLength { get; set; }     // 초점 거리
            public string Iso { get; set; }             // ISO
            public string WhiteBalance { get; set; }    // 화이트밸런스
            public string Datetime { get; set; }        // 촬영 시각
        }
    }
}