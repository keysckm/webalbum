﻿using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using WebAPI.Infrastructure;

namespace WebAPI.Controllers
{
    public class AccountController : ApiController
    {
        private IUserProc _repo = new UserRepo();
        private readonly string _dummyStr = "추가 문자열은 한글입니다.";

        // GET: /api/account/isused
        [HttpGet]
        [ActionName("ISUSED")]
        public HttpResponseMessage IsUsed(string id)
        {
            if (string.IsNullOrEmpty(id))
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            if (IsUsedId(id))
                return new HttpResponseMessage(HttpStatusCode.Forbidden);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // POST: /api/acoount/login
        [HttpPost]
        [ActionName("LOGIN")]
        [ResponseType(typeof(Models.Response.LoginResult))]
        public IHttpActionResult Login(Models.Request.LoginModel model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();

            // 더미문자를 붙이고 해싱한다.
            string dummypw = model.Pw + _dummyStr;
            string sha1 = GetSha1String(dummypw);

            var user = _repo.Get(model.Id);
            if (user == null || user.Pw != sha1)
                return NotFound();

            user.Token = TokenUtil.GenerateToken();
            _repo.Update(user);

            var result = new Models.Response.LoginResult()
            {
                Idx = user.Idx,
                Id = user.Id,
                Nickname = user.Nickname,
                Token = user.Token,
            };

            return Ok(result);
        }
        
        // POST: /api/acoount/autologin
        [HttpPost]
        [ActionName("AUTOLOGIN")]
        [ResponseType(typeof(Models.Response.LoginResult))]
        public IHttpActionResult AutoLogin(Models.Request.BasicAuthModel model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();
            
            var user = _repo.Get(model.Id);
            if (user == null || user.Token != model.Token)
                return NotFound();

            user.Token = TokenUtil.GenerateToken();
            _repo.Update(user);

            var result = new Models.Response.LoginResult()
            {
                Idx = user.Idx,
                Id = user.Id,
                Nickname = user.Nickname,
                Token = user.Token,
            };

            return Ok(result);
        }

        // POST: /api/account/logout
        [HttpPost]
        [ActionName("LOGOUT")]
        public HttpResponseMessage Logout(Models.Request.BasicAuthModel model)
        {
            if (model == null || !ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            var user = _repo.Get(model.Id);
            if (user == null || user.Token != model.Token)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            user.Token = null;
            _repo.Update(user);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // POST /api/account/create
        [HttpPost]
        [ActionName("CREATE")]
        public HttpResponseMessage CreateUser(Models.Request.UserModel model)
        {
            if (model == null || !ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            // 동일 아이디가 존재하는지 확인한다.
            if (IsUsedId(model.Id))
                return new HttpResponseMessage(HttpStatusCode.Forbidden);

            // 더미문자를 붙이고 해싱한다.
            string dummypw = model.Pw + _dummyStr;
            string sha1 = GetSha1String(dummypw);

            var user = new User()
            {
                Id = model.Id,
                Pw = sha1,
                Nickname = model.Nickname,
            };

            _repo.Add(user);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        #region NonAction Method
        [NonAction]
        private byte[] GetSha1(string str)
        {
            var sha1 = SHA1.Create();
            return sha1.ComputeHash(Encoding.UTF8.GetBytes(str));
        }

        [NonAction]
        private string GetSha1String(string str)
        {
            var sb = new StringBuilder();
            foreach (var b in GetSha1(str))
            {
                sb.Append(b.ToString("X2"));
            }
            return sb.ToString();
        }

        [NonAction]
        private bool IsUsedId(string id)
        {
            return _repo.Get(id) != null;
        }
        #endregion
    }
}
