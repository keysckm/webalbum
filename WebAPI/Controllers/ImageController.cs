﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using Domain.Exception;
using WebAPI.Exception;
using WebAPI.Infrastructure;
using WebAPI.Models.Request;

namespace WebAPI.Controllers
{
    public class ImageController : ApiController
    {
        private IUserProc _uproc = new UserRepo();
        private IAlbumProc _aproc = new AlbumRepo();
        private IPictureProc _pproc = new PictureRepo();

        // POST: /api/image/imageinfo
        [HttpPost]
        [ActionName("imageinfo")]
        [ResponseType(typeof(IEnumerable<Models.Response.ImageInfo>))]
        public IHttpActionResult GetForAlbum(Models.Request.GetImageInfos model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();

            // 인증
            var useridx = _uproc.Auth(model.Id, model.Token);
            if (useridx == null)
                return Unauthorized();

            var album = _aproc.GetForOwner(useridx.Value);
            if (album == null)
                return NotFound();

            var infos = (IEnumerable<Models.Response.ImageInfo>)_pproc.GetForAlbum(model.AlbumIdx)
                .Select(a => new Models.Response.ImageInfo()
                {
                    Idx = a.Idx,
                    Comment = a.Comment,
                    ThumbnailIdx = a.ThumbnailIdx ?? 0,
                    Width = a.Width ?? 0,
                    Height = a.Height ?? 0,
                    FileName = a.FileName,
                    FileSize = a.FileSize,
                }).ToList();

            return Ok(infos);
        }

        // POST: api/image/image
        [HttpPost]
        [ActionName("image")]
        public HttpResponseMessage GetImage(Models.Request.GetImage model)
        {
            if (model == null || !ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            // 인증
            var useridx = _uproc.Auth(model.Id, model.Token);
            if (useridx == null)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            // 데이터 검색
            var raw = _pproc.GetImageBinary(model.ImageIdx);
            if (raw == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            // 권한 확인
            if (!_pproc.IsOwnImage(useridx.Value, raw.Idx))
                return new HttpResponseMessage(HttpStatusCode.Forbidden);
            
            // response 생성
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(raw.Data))
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(raw.MediaType);
            response.Content.Headers.ContentLength = raw.Data.Length;

            return response;
        }

        // POST: api/image/thumbnail
        [HttpPost]
        [ActionName("thumbnail")]
        public HttpResponseMessage GetThumbnail(Models.Request.GetImage model)
        {
            if (model == null || !ModelState.IsValid)
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            // 인증
            var useridx = _uproc.Auth(model.Id, model.Token);
            if (useridx == null)
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            // 데이터 검색
            var raw = _pproc.GetThumbnail(model.ImageIdx);
            if (raw == null)
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            //// 권한 확인
            //if (!_pproc.IsOwnImage(useridx.Value, raw.Idx))
            //    return new HttpResponseMessage(HttpStatusCode.Forbidden);

            // response 생성
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(raw.Data))
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            response.Content.Headers.ContentLength = raw.Data.Length;

            return response;
        }

        // POST: api/image/exif
        [HttpPost]
        [ActionName("exif")]
        [ResponseType(typeof(Models.Response.ExifInfo))]
        public IHttpActionResult GetExif(Models.Request.GetExif model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();

            // 인증
            var useridx = _uproc.Auth(model.Id, model.Token);
            if (useridx == null)
                return Unauthorized();

            // 데이터 검색
            var exif = _pproc.GetExif(model.ImageIdx);
            if (exif == null)
                return NotFound();

            // 권한 확인
            if (!_pproc.IsOwnExif(useridx.Value, exif.Idx))
                return new StatusCodeResult(HttpStatusCode.Forbidden, Request);

            var exifinfo = new Models.Response.ExifInfo()
            {
                Idx = exif.Idx,
                Latitude = exif.Latitude,
                Longitude = exif.Longitude,
                CameraMaker = exif.CameraMaker,
                CameraModel = exif.CameraModel,
                LensMaker = exif.LensMake,
                LensModel = exif.LensModel,
                Orientation = exif.Orientation,
                ExposureTime = exif.ExposureTime,
                FNumber = exif.FNumber,
                MeteringMode = exif.MeteringMode,
                FocalLength = exif.FocalLength,
                Iso = exif.Iso,
                WhiteBalance = exif.WhiteBalance,
                Datetime = exif.Datetime?.ToString("yyyy-MM-dd HH:mm:ss")
            };

            return Ok(exifinfo);
        }

        // POST: /api/image/upload
        // Content-Type: multipart/form-data
        [HttpPost]
        [ActionName("upload")]
        public async Task<HttpResponseMessage> Upload()
        {
            var imgList = new List<ImageData>();
            var gen = new EntityGenerator<UploadPicture>();
            try
            {
                // Model Generate
                var model = await gen.FromMultipartFormData(Request.Content, typeof (UploadPicture));

                // 인증
                int? userIdx = _uproc.Auth(model.Id, model.Token);
                if (userIdx == null)
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);

                // 권한 확인
                var album = _aproc.Get(model.Album);
                if (album == null)
                    return new HttpResponseMessage(HttpStatusCode.NotFound);
                if (album.Owner != userIdx.Value)
                    return new HttpResponseMessage(HttpStatusCode.Forbidden);

                foreach (var file in model.Files)
                {
                    // exif 추출
                    Exif exif = null;
                    try
                    {
                        exif = ExifGenerator.Generate(file);
                    }
                    catch (NotSupportedMediaTypeException ex)
                    {
                        exif = null;
                    }

                    // thumbnail 생성
                    Thumbnail t = CreateThumbnail(file.Data);

                    var data = new ImageData
                    {
                        Comment = model.Comment,
                        FileName = file.FileName,
                        FileSize = file.FileSize,
                        MediaType = file.MediaType,
                        Width = file.Width,
                        Height = file.Height,
                        Data = file.Data,
                        AlbumIdx = album.Idx,
                        __exif = exif,
                        __thumbnail = t
                    };

                    imgList.Add(data);
                }

            }
            catch (NotSupportedMimeTypeException ex)
            {
                return new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
            }
            catch (NotSupportedMediaTypeException ex)
            {
                return new HttpResponseMessage(HttpStatusCode.UnsupportedMediaType);
            }
            
            _pproc.Add(imgList);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        [ActionName("UpdateExif")]
        public IHttpActionResult UpdateExif(Models.Request.UpdateExif model)
        {
            bool isCreate = false;

            Exif exif = null;
            try
            {
                exif = _pproc.GetExif(model.ImageIdx);
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest();
            }

            if (exif == null)
            {
                isCreate = true;
                exif = new Exif();
            }

            Type entityType = exif.GetType();
            Type modelType = model.GetType();
            foreach (var property in modelType.GetProperties())
            {
                if (property.Name == "ImageIdx")
                    continue;

                if (property.Name == "DateTime")
                {

                    continue;
                }

                var data = property.GetValue(model);
                if (data == null) continue;

                var entityProperty = entityType.GetProperties().SingleOrDefault(type => type.Name == property.Name);
                if (entityProperty?.PropertyType == typeof (DateTime?))
                {
                    string strDateTime = (string) property.GetValue(model);
                    DateTime datetime = DateTime.ParseExact(strDateTime, "yyyy-MM-dd HH:mm:ss",
                            CultureInfo.InvariantCulture);

                    entityProperty.SetValue(exif, datetime);
                }
                else
                {
                    entityProperty?.SetValue(exif, data);
                }
            }

            if (isCreate)
            {
                ImageData data = _pproc.Get(model.ImageIdx);
                data.__exif = exif;
                _pproc.UpdateImageData(data);
            }
            else
            {
                _pproc.UpdateExif(exif);
            }

            return Ok();
        }

        [HttpPost]
        [ActionName("Remove")]
        public IHttpActionResult RemoveImage(Models.Request.GetImage model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            int? owner = _uproc.Auth(model.Id, model.Token);
            if (owner == null)
                return Unauthorized();

            if (!_pproc.IsOwnImage(owner.Value, model.ImageIdx))
                return new StatusCodeResult(HttpStatusCode.Forbidden, this);

            _pproc.Remove(model.ImageIdx);
            return Ok();
        }

        [NonAction]
        public Thumbnail CreateThumbnail(byte[] raw)
        {
            Thumbnail t = null;
            using (MemoryStream stream = new MemoryStream(raw))
            {
                Bitmap bitmap = new Bitmap(stream);
                int thumb_width;
                int thumb_height;
                if (bitmap.Width > bitmap.Height)
                {
                    thumb_width = 240;
                    thumb_height = (bitmap.Height * 240) / bitmap.Width;
                }
                else if (bitmap.Width < bitmap.Height)
                {
                    thumb_height = 240;
                    thumb_width = (bitmap.Width * 240) / bitmap.Height;
                }
                else
                {
                    thumb_width = thumb_height = 240;
                }
                var thumbnail = bitmap.GetThumbnailImage(thumb_width, thumb_height, null, IntPtr.Zero);
                using (MemoryStream ms = new MemoryStream())
                {

                    thumbnail.Save(ms, ImageFormat.Jpeg);
                    byte[] thumbnail_raw = ms.ToArray();
                    t = new Thumbnail
                    {
                        Width = thumb_width,
                        Height = thumb_height,
                        Data = thumbnail_raw
                    };
                }
            }
            return t;
        }
    }
}
