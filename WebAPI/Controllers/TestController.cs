﻿#if DEBUG
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Domain.Entities;
using WebAPI.Infrastructure;
using WebAPI.Models.Request;

namespace WebAPI.Controllers
{
    public class TestController : ApiController
    {
        [HttpPost]
        [ActionName("UPLOAD")]
        public async Task<IHttpActionResult> Upload()
        {
            Repository repo = new Repository();
            var users = repo.Users.Any();
            var gen = new EntityGenerator<TestModel>();
            var model = await gen.FromMultipartFormData(Request.Content, typeof(TestModel));

            if (model != null)
                return Ok();
            else
                return InternalServerError();
        }
    }
}

#endif