﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Entities;
using WebAPI.Exception;
using WebAPI.Infrastructure;
using WebAPI.Models.Request;

namespace WebAPI.Controllers
{
    public class AlbumController : ApiController
    {
        private IAlbumProc _aproc = new AlbumRepo();
        private IUserProc _uproc = new UserRepo();
        private IPictureProc _pproc = new PictureRepo();
        
        // POST: /api/album/albuminfo
        [HttpPost]
        [ActionName("albuminfo")]
        [ResponseType(typeof(IEnumerable<Models.Response.AlbumModel>))]
        public IHttpActionResult GetForOwner(Models.Request.BasicAuthModel model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();

            // 인증
            int? userIdx = _uproc.Auth(model.Id, model.Token);
            if (userIdx == null)
                return Unauthorized();

            // 앨범들을 가져온다.
            var albums = _aproc.GetForOwner(userIdx.Value);
            if (!albums.Any())
                return NotFound();  // 없다.

            // 뷰모델로 변환한다.
            var models = (IEnumerable<Models.Response.AlbumModel>)albums.Reverse().Select(a => new Models.Response.AlbumModel()
            {
                Idx = a.Idx,
                Title = a.Title,
                ImageCount = _pproc.GetForAlbum(a.Idx).Count(),
                Date = a.Date.ToString("yyyy-MM-dd HH:mm:ss"),
                Thumbnail = a.Thumbnail ?? -1
            }).ToList();

            return Ok(models);
        }

        //// POST: /api/album/create
        //[HttpPost]
        //[ActionName("Create")]
        //public IHttpActionResult Create(Models.Request.CreateAlbum model)
        //{
        //    if (model == null || !ModelState.IsValid)
        //        return BadRequest();
            
        //    // 인증
        //    int? userIdx = _uproc.Auth(model.Id, model.Token);
        //    if (userIdx == null)
        //        return Unauthorized();

        //    var album = new Album()
        //    {
        //        Title = model.Title,
        //        Owner = userIdx.Value,
        //        Date = DateTime.Now,
        //    };

        //    _aproc.Add(album);
        //    return Ok();
        //}

        // POST: /api/album/create
        [HttpPost]
        [ActionName("create")]
        public async Task<IHttpActionResult> Create()
        {
            try
            {
                var gen = new EntityGenerator<CreateAlbum>();
                // Multipart/form-data형식의 요청에서 각 데이터를 가져온다.
                var model = await gen.FromMultipartFormData(Request.Content, typeof (CreateAlbum));
                if (model == null)
                    return InternalServerError();

                int? userIdx = _uproc.Auth(model.Id, model.Token);
                if (userIdx == null)
                    return Unauthorized();

                var album = new Album
                {
                    Title = model.Title,
                    Owner = userIdx.Value,
                    Date = DateTime.Now
                };

                if (model.Files.Any()) // thumbnail이 있으면 함께 등록한다.
                {
                    var file = model.Files.First();
                    var thumbnail = new Thumbnail
                    {
                        Width = file.Width,
                        Height = file.Height,
                        Data = file.Data
                    };
                    album.__thumbnail = thumbnail;
                }
                _aproc.Add(album);
            }
            catch (NotSupportedMimeTypeException ex)
            {
                return StatusCode(HttpStatusCode.MethodNotAllowed);
            }
            catch (NotSupportedMediaTypeException ex)
            {
                return StatusCode(HttpStatusCode.UnsupportedMediaType);
            }
            catch (IOException ex)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }

            return Ok();
        }

        // POST: /api/album/modify
        [HttpPost]
        [ActionName("modify")]
        public IHttpActionResult Modify(Models.Request.ModifyAlbum model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest();

            // 인증
            int? userIdx = _uproc.Auth(model.Id, model.Token);
            if (userIdx == null)
                return Unauthorized();

            var album = _aproc.Get(model.Idx);
            if (album == null)
                return NotFound();

            // 권한 확인
            if (album.Owner != userIdx.Value)
                return new StatusCodeResult(HttpStatusCode.Forbidden, Request);

            album.Title = model.Title;
            _aproc.Update(album);
            return Ok();
        }
    }
}
