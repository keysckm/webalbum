﻿using System;
using System.Linq;
using Domain.Concrete;
using Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebAPI.Controllers;
using WebAPI.Models.Request;

namespace WebAPI.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateNewExifTest()
        {
            // 테스트 준비
            PictureRepo repo = new PictureRepo();
            ImageData data = repo.Get(1);
            Exif exif = new Exif();
            exif.Latitude = 3.0f;
            exif.Longitude = 2.5f;

            // 테스트 작업 
            data.__exif = exif;
            repo.UpdateImageData(data);
            
            // 검증
            Assert.IsNotNull(data.ExifIdx);
        }

        [TestMethod]
        public void UpdateExifTest()
        {
            // 테스트 준비
            PictureRepo repo = new PictureRepo();
            ImageController controller = new ImageController();
            int? originalExifIdx = repo.Get(1).ExifIdx;

            UpdateExif model = new UpdateExif();
            model.ImageIdx = 1;
            model.CameraMaker = "SOOONY";
            model.CameraModel = "TEST";
            model.Datetime = "1990-07-26 22:11:01";

            // 테스트 실행
            controller.UpdateExif(model);

            // 결과
            ImageData data = repo.Get(1);
            Exif exif = repo.GetExif(1);
            Assert.IsNotNull(exif, "exif row가 존재해야 한다.");
            Assert.AreEqual(data.ExifIdx, originalExifIdx, "Exif는 새로 생성되거나 다른 컬럼으로 변경되지 않아야 한다.");
            Assert.IsNotNull(exif.Latitude, "변경을 의도하지 않은 Latitude Column 데이터는 살아있어야 한다.");
            Assert.IsNotNull(exif.Longitude, "변경을 의도하지 않은 Longitude Column 데이터는 살아있어야 한다.");
            Assert.AreEqual(exif.CameraMaker, "SOOONY", "데이터는 제대로 등록되어야 한다.");
            Assert.AreEqual(exif.CameraModel, "TEST", "데이터는 제대로 등록되어야 한다.");
            Assert.AreEqual(new DateTime(1990, 7, 26, 22, 11, 01), exif.Datetime, "DateTime 변환이 잘 되어야 한다.");
        }

        [TestMethod]
        public void RemoveImageTest()
        {
            // 준비
            Repository repository = new Repository();
            PictureRepo repo = new PictureRepo();
            ImageData data = repo.Get(91);
            int exifidx = data.ExifIdx.Value;
            int thumbidx = data.ThumbnailIdx.Value;

            // 실행
            repo.Remove(data.Idx);

            // 검증
            Assert.IsNull(repo.Get(91));
            Assert.IsNull(repository.Thumbnails.SingleOrDefault(model => model.Idx == thumbidx));
            Assert.IsNull(repository.Exifs.SingleOrDefault(model => model.Idx == exifidx));
        }
    }
}
