﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class AlbumRepo : BaseEntity, IAlbumProc
    {
        public Album Get(int idx)
        {
            return _repo.Albums.FirstOrDefault(model => model.Idx == idx);
        }

        public ImageData GetThumbnail(int idx)
        {
            var album = Get(idx);
            if (album?.Thumbnail == null)
                return null;

            return _repo.ImageDatas.SingleOrDefault(i => i.Idx == album.Thumbnail);
        }

        public void SetThumbnail(int idx, int thumbnail)
        {
            var album = Get(idx);
            if (album == null)
                return;

            album.Thumbnail = thumbnail;
            Update(album);
        }

        public IEnumerable<Album> GetForOwner(int owner)
        {
            return _repo.Albums.Where(model => model.Owner == owner);
        }

        public void Add(Album album)
        {
            // thumbnail이 존재하면 트랜젝션에 추가한다.
            if (album.__thumbnail != null)
                _repo.Thumbnails.Add(album.__thumbnail);

            _repo.Albums.Add(album);
            _repo.SaveChanges();
        }

        public void Update(Album album)
        {
            _repo.Entry(album).State = EntityState.Modified;
            _repo.SaveChanges();
        }

        public void Remove(int idx)
        {
            var model = Get(idx);
            if (model != null)
                _repo.Albums.Remove(model);
        }
    }
}
