﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;
using Domain.Exception;

namespace Domain.Concrete
{
    public class PictureRepo : BaseEntity, IPictureProc
    {
        public ImageData Get(int idx)
        {
            return _repo.ImageDatas.FirstOrDefault(model => model.Idx == idx);
        }

        public IEnumerable<ImageData> GetForAlbum(int albumIdx)
        {
            return _repo.ImageDatas.Where(e => e.AlbumIdx == albumIdx);
        }

        public void UpdateImageData(ImageData model)
        {
            _repo.Entry(model).State = EntityState.Modified;
            _repo.SaveChanges();
        }

        public Exif GetExif(int imageIdx)
        {
            ImageData image = _repo.ImageDatas.SingleOrDefault(model => model.Idx == imageIdx);
            if (image == null)
                throw new EntityNotFoundException();

            return _repo.Exifs.FirstOrDefault(model => model.Idx == image.ExifIdx);
        }

        public void UpdateExif(Exif model)
        {
            _repo.Entry(model).State = EntityState.Modified;
            _repo.SaveChanges();
        }

        public bool IsOwnExif(int owner, int idx)
        {
            var view = from i in _repo.ImageDatas
                       join a in _repo.Albums on i.AlbumIdx equals a.Idx
                       join u in _repo.Users on a.Owner equals u.Idx
                       where i.ExifIdx == idx && u.Idx == owner
                       select new
                       {
                           owner = u.Idx,
                           album = a.Idx,
                           img = i.Idx
                       };

            return view.Any();
        }

        public Thumbnail GetThumbnail(int idx)
        {
            return _repo.Thumbnails.SingleOrDefault(model => model.Idx == idx);
        }

        public ImageData GetImageBinary(int idx)
        {
            return _repo.ImageDatas.SingleOrDefault(model => model.Idx == idx);
        }

        public bool IsOwnImage(int owner, int idx)
        {
            var view = from i in _repo.ImageDatas
                       join a in _repo.Albums on i.AlbumIdx equals a.Idx
                       join u in _repo.Users on a.Owner equals u.Idx
                       where i.Idx == idx && u.Idx == owner
                       select new
                       {
                           owner = u.Idx,
                           album = a.Idx,
                           img = i.Idx,
                       };

            return view.Any();
        }

        public void Add(List<ImageData> imgs)
        {
            foreach (var img in imgs)
            {
                if (img.__exif != null)
                    _repo.Exifs.Add(img.__exif);

                _repo.ImageDatas.Add(img);
            }

            _repo.SaveChanges();
        }

        public void Remove(int imginfoIdx)
        {
            // 지금은 exif와 thumbnail은 cascade로 삭제되지 않는다.
            // 따라서 직접 삭제해주어야 함.

            ImageData imagedata = _repo.ImageDatas.SingleOrDefault(model => model.Idx == imginfoIdx);
            if (imagedata == null)
                return;

            Exif exif = null;
            Thumbnail thumbnail = null;

            if (imagedata.ExifIdx != null)
                exif = _repo.Exifs.SingleOrDefault(model => model.Idx == imagedata.ExifIdx);

            if (imagedata.ThumbnailIdx != null)
                thumbnail = _repo.Thumbnails.SingleOrDefault(model => model.Idx == imagedata.ThumbnailIdx);

            _repo.ImageDatas.Remove(imagedata);
            _repo.Thumbnails.Remove(thumbnail);
            _repo.Exifs.Remove(exif);
            _repo.SaveChanges();
        }
    }
}
