﻿using System.Data.Entity;
using System.Linq;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class UserRepo : BaseEntity, IUserProc
    {
        public User Get(string id)
        {
            return _repo.Users.SingleOrDefault(model => model.Id == id);
        }

        public int? Auth(string id, string token)
        {
            var user = _repo.Users.SingleOrDefault(u => u.Id == id && u.Token == token);
            return user?.Idx;
        }

        public void Add(User user)
        {
            _repo.Users.Add(user);
            _repo.SaveChanges();
        }

        public void Update(User user)
        {
            _repo.Entry(user).State = EntityState.Modified;
            _repo.SaveChanges();
        }

        public void Remove(int idx)
        {
            var u = _repo.Users.SingleOrDefault(model => model.Idx == idx);
            if (u != null)
                _repo.Users.Remove(u);
        }
    }
}
