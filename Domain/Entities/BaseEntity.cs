﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class BaseEntity : IDisposable
    {
        protected Repository _repo = new Repository();

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
