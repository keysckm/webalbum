﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("ImageData")]
    public class ImageData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Idx { get; set; }    // 식별자

        public string Comment { get; set; }

        [Required]
        [StringLength(255)]
        public string FileName { get; set; }

        public int FileSize { get; set; }

        [Required]
        public string MediaType { get; set; }

        public int? Width { get; set; }  // 이미지 가로길이

        public int? Height { get; set; } // 이미지 세로길이

        [Required]
        public byte[] Data { get; set; }

        public int? ExifIdx { get; set; }   // Exif idx

        public int? ThumbnailIdx { get; set; }  // thumbnail idx

        public int AlbumIdx { get; set; }  // 소속 앨범

        [ForeignKey("AlbumIdx")]
        public Album __album { get; set; }

        [ForeignKey("ExifIdx")]
        public Exif __exif { get; set; }

        [ForeignKey("ThumbnailIdx")]
        public Thumbnail __thumbnail { get; set; }
    }
}
