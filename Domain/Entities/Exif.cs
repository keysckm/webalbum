﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("Exif")]
    public class Exif
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Idx { get; set; }           // 식별자

        public float? Latitude { get; set; }    // 위도

        public float? Longitude { get; set; }   // 경도

        public string CameraMaker { get; set; }     // 카메라 제조사

        public string CameraModel { get; set; }     // 카메라 모델

        public string LensMake { get; set; }        // 렌즈 제조사

        public string LensModel { get; set; }       // 렌즈 모델

        public string Orientation { get; set; }     // 사진 방향

        public string ExposureTime { get; set; }     // 셔터스피드

        public string FNumber { get; set; }          // 조리개

        public string MaxApertureValue { get; set; } // 조리개 최대 개방

        public string FlashMode { get; set; }       // 플래시 모드

        public string MeteringMode { get; set; }    // 측광 모드

        public string FocalLength { get; set; }      // 초점 거리

        public string Iso { get; set; }        // ISO

        public string WhiteBalance { get; set; }    // 화이트밸런스

        public DateTime? Datetime { get; set; }  // 촬영 시각
    }
}
