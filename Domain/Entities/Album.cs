﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    [Table("Album")]
    public class Album
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Idx { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        
        public int Owner { get; set; }
    
        public DateTime Date { get; set; }

        public int? Thumbnail { get; set; }

        [ForeignKey("Owner")]
        public User __owner { get; set; }

        [ForeignKey("Thumbnail")]
        public Thumbnail __thumbnail { get; set; }
    }
}
