﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    [Table("User")]
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Idx { get; set; }

        [Required]
        [StringLength(64)]
        [MinLength(8)]
        [EmailAddress]
        public string Id { get; set; }

        [Required]
        [StringLength(40)]  // 해싱된 패스워드의 문자열 길이는 40이다.
        public string Pw { get; set; }

        public string Token { get; set; }

        [Required]
        [StringLength(16)]
        public string Nickname { get; set; }
    }
}
