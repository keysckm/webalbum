namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_exif_datetime_nullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Exif", "Datetime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Exif", "Datetime", c => c.DateTime(nullable: false));
        }
    }
}
