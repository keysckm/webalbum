namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_imagedata_add_thumbnail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageData", "ThumbnailIdx", c => c.Int());
            CreateIndex("dbo.ImageData", "ThumbnailIdx");
            AddForeignKey("dbo.ImageData", "ThumbnailIdx", "dbo.Thumbnail", "Idx");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ImageData", "ThumbnailIdx", "dbo.Thumbnail");
            DropIndex("dbo.ImageData", new[] { "ThumbnailIdx" });
            DropColumn("dbo.ImageData", "ThumbnailIdx");
        }
    }
}
