// <auto-generated />
namespace Domain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class modify_exif_datetime_nullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(modify_exif_datetime_nullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201510251922063_modify_exif_datetime_nullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
