namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCommentColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ImageData", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ImageData", "Comment");
        }
    }
}
