namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Album",
                c => new
                    {
                        Idx = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Owner = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Thumbnail = c.Int(),
                    })
                .PrimaryKey(t => t.Idx)
                .ForeignKey("dbo.User", t => t.Owner, cascadeDelete: true)
                .ForeignKey("dbo.Thumbnail", t => t.Thumbnail)
                .Index(t => t.Owner)
                .Index(t => t.Thumbnail);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Idx = c.Int(nullable: false, identity: true),
                        Id = c.String(nullable: false, maxLength: 64),
                        Pw = c.String(nullable: false, maxLength: 40),
                        Token = c.String(),
                        Nickname = c.String(nullable: false, maxLength: 16),
                    })
                .PrimaryKey(t => t.Idx);
            
            CreateTable(
                "dbo.Thumbnail",
                c => new
                    {
                        Idx = c.Int(nullable: false, identity: true),
                        Width = c.Int(),
                        Height = c.Int(),
                        Data = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Idx);
            
            CreateTable(
                "dbo.Exif",
                c => new
                    {
                        Idx = c.Int(nullable: false, identity: true),
                        Latitude = c.Single(),
                        Longitude = c.Single(),
                        CameraMaker = c.String(),
                        CameraModel = c.String(),
                        LensMake = c.String(),
                        LensModel = c.String(),
                        Orientation = c.String(),
                        ExposureTime = c.String(),
                        FNumber = c.String(),
                        MaxApertureValue = c.String(),
                        FlashMode = c.String(),
                        MeteringMode = c.String(),
                        FocalLength = c.String(),
                        Iso = c.String(),
                        WhiteBalance = c.String(),
                        Datetime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Idx);
            
            CreateTable(
                "dbo.ImageData",
                c => new
                    {
                        Idx = c.Int(nullable: false, identity: true),
                        FileName = c.String(nullable: false, maxLength: 255),
                        FileSize = c.Int(nullable: false),
                        MediaType = c.String(nullable: false),
                        Width = c.Int(),
                        Height = c.Int(),
                        Data = c.Binary(nullable: false),
                        ExifIdx = c.Int(),
                        AlbumIdx = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Idx)
                .ForeignKey("dbo.Album", t => t.AlbumIdx, cascadeDelete: true)
                .ForeignKey("dbo.Exif", t => t.ExifIdx)
                .Index(t => t.ExifIdx)
                .Index(t => t.AlbumIdx);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ImageData", "ExifIdx", "dbo.Exif");
            DropForeignKey("dbo.ImageData", "AlbumIdx", "dbo.Album");
            DropForeignKey("dbo.Album", "Thumbnail", "dbo.Thumbnail");
            DropForeignKey("dbo.Album", "Owner", "dbo.User");
            DropIndex("dbo.ImageData", new[] { "AlbumIdx" });
            DropIndex("dbo.ImageData", new[] { "ExifIdx" });
            DropIndex("dbo.Album", new[] { "Thumbnail" });
            DropIndex("dbo.Album", new[] { "Owner" });
            DropTable("dbo.ImageData");
            DropTable("dbo.Exif");
            DropTable("dbo.Thumbnail");
            DropTable("dbo.User");
            DropTable("dbo.Album");
        }
    }
}
