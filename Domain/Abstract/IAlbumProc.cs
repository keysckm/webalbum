﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IAlbumProc
    {
        /// <summary>
        /// 데이터베이스에서 Album 정보를 가져옵니다.
        /// </summary>
        /// <param name="idx">Album의 Idx입니다.</param>
        /// <returns>Album 객체입니다. 찾을 수 없다면 null입니다.</returns>
        Album Get(int idx);

        /// <summary>
        /// Album의 Thumbnail 이미지의 Raw데이터를 가져옵니다.
        /// </summary>
        /// <param name="idx">Thumbnail 이미지를 가져올 Album의 식별자입니다.</param>
        /// <returns>Thumbnail image의 raw 데이터입니다. 찾을 수 없다면 null입니다.</returns>
        ImageData GetThumbnail(int idx);

        /// <summary>
        /// Album에 Thumbnail을 등록합니다.
        /// </summary>
        /// <param name="idx">Thumbnail을 등록할 Album의 식별자입니다.</param>
        /// <param name="thumbnail">thumbnail 이미지의 ImageRaw 테이블상 식별자입니다.</param>
        void SetThumbnail(int idx, int thumbnail);

        /// <summary>
        /// 유저가 소유하고 있는 Album정보들을 가져옵니다.
        /// </summary>
        /// <param name="owner">유저의 idx입니다.</param>
        /// <returns>Album의 제네릭 컬랙션입니다.</returns>
        IEnumerable<Album> GetForOwner(int owner); 

        /// <summary>
        /// 데이터베이스에 새로운 Album 정보를 추가합니다.
        /// </summary>
        /// <param name="album">추가 할 Album의 데이터입니다.</param>
        void Add(Album album);

        /// <summary>
        /// 데이터베이스의 Album정보를 수정합니다.
        /// </summary>
        /// <param name="album">수정 될 Album 정보입니다.</param>
        void Update(Album album);

        /// <summary>
        /// 데이터베이스에서 Album정보를 삭제합니다.
        /// </summary>
        /// <param name="idx">삭제 할 Album의 Idx입니다.</param>
        void Remove(int idx);
    }
}
