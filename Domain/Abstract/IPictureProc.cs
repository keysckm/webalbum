﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IPictureProc
    {
        /// <summary>
        /// ImageInfo 테이블에서 데이터를 가져옵니다.
        /// </summary>
        /// <param name="idx">가져올 데이터의 인덱스입니다.</param>
        /// <returns>ImageInfo Model입니다. 찾을 수 없었다면 null입니다.</returns>
        ImageData Get(int idx);

        /// <summary>
        /// ImageInfo 테이블에서 데이터를 가져옵니다.
        /// </summary>
        /// <param name="albumIdx">ImageInfo Column이 소속된 album의 인덱스입니다.</param>
        /// <returns>ImageInfo의 IEnumerable 제네릭입니다.</returns>
        IEnumerable<ImageData> GetForAlbum(int albumIdx);

        /// <summary>
        /// ImageData 데이터를 업데이트합니다.
        /// </summary>
        /// <param name="model">업데이트 할 ImageData 데이터입니다.</param>
        void UpdateImageData(ImageData model);

        /// <summary>
        /// Exif 테이블에서 데이터를 가져옵니다.
        /// </summary>
        /// <param name="imageIdx">Exif데이터를 가지고있는 실제 이미지의 인덱스입니다.</param>
        /// <returns>Exif Model입니다. 찾을 수 없었다면 null입니다.</returns>
        Exif GetExif(int imageIdx);

        /// <summary>
        /// Exif 데이터를 업데이트합니다.
        /// </summary>
        /// <param name="model">업데이트 할 Exif 데이터입니다.</param>
        void UpdateExif(Exif model);
        
        /// <summary>
        /// Exif 데이터에 대한 권한을 검사합니다.
        /// </summary>
        /// <param name="owner">소유자의 idx입니다.</param>
        /// <param name="idx">exif의 idx입니다.</param>
        /// <returns>권한이 올바르다면 true, 그렇지 않으면 false입니다.</returns>
        bool IsOwnExif(int owner, int idx);

        /// <summary>
        /// Thumbnail 테이블에서 데이터를 가져옵니다.
        /// </summary>
        /// <param name="idx">가져올 데이터의 인덱스입니다.</param>
        /// <returns>Thumbnail 모델입니다. 찾을 수 없었다면 null입니다.</returns>
        Thumbnail GetThumbnail(int idx);

        /// <summary>
        /// Image 테이블에서 데이터를 가져옵니다.
        /// </summary>
        /// <param name="idx">가져올 데이터의 인덱스입니다.</param>
        /// <returns>Image 모델입니다. 찾을 수 없었다면 null입니다.</returns>
        ImageData GetImageBinary(int idx);

        /// <summary>
        /// 이미지에 대한 권한을 검사합니다.
        /// </summary>
        /// <param name="owner">소유자 idx입니다.</param>
        /// <param name="idx">이미지 idx입니다.</param>
        /// <returns>권한이 올바르다면 true, 그렇지 않으면 false입니다.</returns>
        bool IsOwnImage(int owner, int idx);

        /// <summary>
        /// 새로운 이미지를 추가합니다.
        /// </summary>
        /// <param name="imgs">이미지들의 dictionary 컬랙션입니다.</param>
        void Add(List<ImageData> imgs);

        /// <summary>
        /// 이미지를 삭제합니다.
        /// </summary>
        /// <param name="img">삭제할 이미지 정보의 인덱스입니다.</param>
        void Remove(int imginfoIdx);
    }
}
