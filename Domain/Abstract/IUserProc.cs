﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Abstract
{
    public interface IUserProc
    {
        User Get(string id);
        int? Auth(string id, string token);

        void Add(User user);
        void Update(User user);
        void Remove(int idx);
    }
}
