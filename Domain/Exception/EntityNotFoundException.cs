﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Exception
{
    public class EntityNotFoundException : System.Exception
    {
        public EntityNotFoundException()
            : base("Entity Not found")
        {

        }

        public EntityNotFoundException(string msg)
            : base(msg)
        {
            
        }
    }
}
